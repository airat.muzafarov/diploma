package util;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Data;

import java.io.File;
import java.io.IOException;

@Data
public class Config {
    class Device {
        String width;
        String height;
    }

    ObjectMapper objectMapper = new ObjectMapper();
    private JsonNode configNode;
    private String email;
    private String password;
    private String browser;
    private String device;

    private static Config config;

    public static Config getConfig() {
        if (config == null)
            config = new Config();

        return config;
    }

    private Config() {
        {
            try {
                configNode = objectMapper
                        .readTree(new File(getClass().getClassLoader().getResource("uat.json").getPath()));
                if (configNode.has("email"))
                    setEmail(configNode.get("email").asText());
                if (configNode.has("password"))
                    setPassword(configNode.get("password").asText());
                browser = System.getenv("browser") != null ? System.getenv("browser") : "chrome";
                device = System.getenv("device") != null ? System.getenv("device") : "desktop";
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}

