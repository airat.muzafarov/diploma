package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class WomenCategoryPage extends BasePage {

    @FindBy(xpath = "(//h5/a[@itemprop = 'url'])[1]")
    public WebElement firstItem;

    public WomenCategoryPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public ItemPage selectItem() {
        firstItem.click();
        return new ItemPage(driver);
    }
}
