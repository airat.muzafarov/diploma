package pages;

import io.qameta.allure.Allure;
import io.qameta.allure.Step;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class MainPage extends BasePage {
    private final String url = "http://automationpractice.com";
    @FindBy(xpath = "//*[@title = 'Log in to your customer account']")
    public WebElement signInButton;

    @FindBy(xpath = "//*[@id = 'block_contact_infos']")
    public WebElement contactInfo;

    @FindBy(xpath = "//a[@title = 'Women']")
    public WebElement womenCategory;

    public MainPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }
    @Step("Открываем сайт")
    public MainPage open() {
        driver.get(url);
        return this;
    }
    @Step("Переход на страницу авторизации")
    public LoginPage goToLoginPage() {
        waitClickable(signInButton, 10);
        signInButton.click();
        return new LoginPage(driver);
    }

    @Step("Переход на страницу категорий Women")
    public WomenCategoryPage goToWomenCategoryPage() {
        waitClickable(womenCategory, 10);
        womenCategory.click();
        return new WomenCategoryPage(driver);
    }

    @Step("Скрол к контактам")
    public String scrollToContact(){
        scrollTo(contactInfo);
        Allure.getLifecycle().addAttachment("About company", "image/png", "png",
                contactInfo.getScreenshotAs(OutputType.BYTES));
        return contactInfo.getText();
    }
}
