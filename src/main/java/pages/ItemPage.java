package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ItemPage extends BasePage {

    String expectedText = "Product successfully added to your shopping cart";

    @FindBy(xpath = "/html/body/div/div[2]/div/div[3]/div/div/div/div[4]/form/div/div[3]/div/p")
    public WebElement addButton;

    @FindBy(xpath = "//*[@class = 'icon-ok']")
    public WebElement successfullyAdded;

    @FindBy(xpath = "//*[@id='layer_cart']/div[1]/div[1]/h2")
    public WebElement successMessage;

    public ItemPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public ItemPage addToCart() {
     //   waitClickable(addButton, 100);
        addButton.click();
        return this;
    }

    public boolean isAdded() {
        waitVisible(successfullyAdded, 5);
        String actualText = successMessage.getText();
        return expectedText.equals(actualText) && successfullyAdded.isDisplayed();
    }
}
