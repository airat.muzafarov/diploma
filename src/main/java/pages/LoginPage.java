package pages;

import io.qameta.allure.Step;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import ru.yandex.qatools.ashot.comparison.ImageDiff;
import ru.yandex.qatools.ashot.comparison.ImageDiffer;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Objects;

public class LoginPage extends BasePage {
    @FindBy(xpath = "//*[@id = 'email']")
    public WebElement userInputEmail;

    @FindBy(xpath = "//*[@id = 'passwd']")
    public WebElement userInputPasswd;

    @FindBy(xpath = "//*[@name = 'SubmitLogin']")
    public WebElement signInButton;

    @FindBy(xpath = "//*[@class = 'alert alert-danger']//li")
    public WebElement authenticationError;

    @FindBy(xpath = "//*[@id = 'login_form']")
    public WebElement loginBox;

    public LoginPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    @Step("Вводим email и password")
    public AccountPage login(String email, String password) {
        userInputEmail.sendKeys(email);
        userInputPasswd.sendKeys(password);
        signInButton.click();
        return new AccountPage(driver);
    }

    @Step("Вводим email и password")
    public LoginPage wrongLogin(String email, String password) {
        userInputEmail.sendKeys(email);
        userInputPasswd.sendKeys(password);
        signInButton.click();
        return this;
    }

    @Step("Получаем ошибку авторизации")
    public String getLoginErrorMessage() {
        waitVisible(authenticationError, 5);
        return authenticationError.getText();
    }

    @Step("Сравнение скриншотов")
    public boolean checkLoginBoxIsDifferByImage() {
        waitVisible(loginBox, 10);
        try {
            Thread.sleep(5000);
            scrollTo(loginBox);
            BufferedImage screenShot;
            screenShot = ImageIO.read(new ByteArrayInputStream(loginBox.getScreenshotAs(OutputType.BYTES)));
            BufferedImage etalonShot = ImageIO.read(Objects.requireNonNull(Thread.currentThread()
                    .getContextClassLoader().getResourceAsStream("LoginBox.png")));
            ImageDiff diff = new ImageDiffer().makeDiff(screenShot, etalonShot);
            return diff.hasDiff();
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
        return false;
    }
}
