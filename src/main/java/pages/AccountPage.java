package pages;

import io.qameta.allure.Step;
import org.junit.jupiter.api.Assertions;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class AccountPage extends BasePage {
    public WebElement accountName = driver.findElement(By.xpath("//*[@title = 'View my customer account']"));

    public AccountPage(WebDriver driver) {
        super(driver);
    }

    @Step("Проверяем корректность авторизации")
    public void isLogin(String userName) {
        String actualName = accountName.getText();
        Assertions.assertEquals(userName, actualName);
    }
}
