package tests;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import pages.MainPage;

import java.util.stream.Stream;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class LoginTest extends BaseTest {

    @Test
    @DisplayName("Тест на логин юзера")
    public void loginTest() {
        new MainPage(driver)
                .open()
                .goToLoginPage()
                .login(user.getEmail(), user.getPassword())
                .isLogin("Airat Muzafarov");
    }

    @Test
    @DisplayName("Тест на проверку информации о компании")
    void  scrollToContactInfo(){
        assertThat(new MainPage(driver).open().scrollToContact()).isEqualTo("Store information\n" +
                "Selenium Framework, Research Triangle Park, North Carolina, USA\n" +
                "Call us now: (347) 466-7432\n" +
                "Email: support@seleniumframework.com");
    }

    @Tag("screenshot")
    @DisplayName("Тест на сравнения скришотов формы логина.")
    @Test
    void compareImage(){
        assertThat(new MainPage(driver)
                .open()
                .goToLoginPage()
                .checkLoginBoxIsDifferByImage())
                .as("LoginBox is not equal to etalon image")
                .isFalse();
    }

    @ParameterizedTest
    @MethodSource("dataSource")
    @DisplayName("Негативный тест на логин с некорректными данными")
    public void loginWithWrongCredentials(String email, String errMessage) {
        String message = new MainPage(driver)
                .open()
                .goToLoginPage()
                .wrongLogin(email, user.getPassword())
                .getLoginErrorMessage();
        assertEquals(message, errMessage);
    }

    private static Stream<Arguments> dataSource() {
        return Stream.of(
                Arguments.of(" ", "An email address required."),
                Arguments.of("dsfsdf@mail.ru", "Authentication failed.")
        );
    }
}
