package tests;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.openqa.selenium.WebDriver;
import pojo.User;
import util.Config;
import util.TestListener;
import util.WebdriverFactory;

@ExtendWith(TestListener.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class BaseTest {
    public static WebDriver driver;
    Config config = Config.getConfig();
    static User user;

    @BeforeEach
    public void setUp() {
        user = new User(config.getEmail(),config.getPassword());
        if(driver!= null)
            driver.close();
        driver = new WebdriverFactory().create();
    }

    @AfterAll
    public void tearDown() {
        driver.quit();
    }
}
