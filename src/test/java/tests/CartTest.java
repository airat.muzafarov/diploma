package tests;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import pages.MainPage;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class CartTest extends BaseTest {

    @Test
    @DisplayName("Добавление товара в корзину")
    public void addToCart() {
        assertTrue(new MainPage(driver).open()
                .goToWomenCategoryPage()
                .selectItem()
                .addToCart()
                .isAdded());
    }
}
